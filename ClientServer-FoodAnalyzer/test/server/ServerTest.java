package server;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import com.google.gson.Gson;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import server.dto.food_info.FoodInfo;
import server.dto.food_info.GeneralFoodInfo;
import server.dto.food_info.ListInfo;
import server.exception.InvalidFoodRequestException;

import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class ServerTest {

    private static final String API_KEY = "BwcEdAweWxjMiPBZEySuoiw8bEollRWTbOcdDBvk";

    @Mock
    private HttpClient httpClientMock;

    @Mock
    private CompletableFuture<HttpResponse<String>> httpResponseMock;

    private Server serverr;

    @Before
    public void setUp() {
        serverr = new Server(8080, httpClientMock);
        serverr.start();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testGetListWithTwoFoods() {
        when(httpClientMock.sendAsync(Mockito.any(HttpRequest.class), ArgumentMatchers.<HttpResponse.BodyHandler<String>>any()))
                .thenReturn(httpResponseMock);
        GeneralFoodInfo genFood = new GeneralFoodInfo(new ListInfo(List.of(new FoodInfo("Java meetup", "dw", "dwqf"),
                new FoodInfo("Java meetup", "dw", "dwqf"))));
        String json = new Gson().toJson(genFood);
        when(httpResponseMock.join()).thenReturn((HttpResponse<String>) genFood.getList());
        System.out.println(httpResponseMock.join().body());
        List<FoodInfo> actual = serverr.getFoodInfo(API_KEY, "t423est");
        assertEquals(2, actual.size());
        assertEquals("Java meetup", actual.get(0).getName());
    }

    @Test
    public void getFoodReport() {
    }

    @Test
    public void getFoodByBarcode() {
    }

    @Test
    public void getFoodByBarcodeImage() {
    }
}