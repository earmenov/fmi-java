package client;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;

// https://api.nal.usda.gov/ndb/reports/?ndbno=45142036&type=f&api_key=BwcEdAweWxjMiPBZEySuoiw8bEollRWTbOcdDBvk
// https://api.nal.usda.gov/ndb/search/?q=raffaello&api_key=BwcEdAweWxjMiPBZEySuoiw8bEollRWTbOcdDBvk

public class Client {

    private static final String SUCCESSFUL_CONNECTION = "Successfully connected to the server on port %d";
    private static final String SERVER_NOT_FOUND_ERROR = "Server not found: ";

    private String hostname;
    private String apiKey;
    private int port;

    Client(String hostname, int port, String apiKey) {
        this.hostname = hostname;
        this.port = port;
        this.apiKey = apiKey;
    }

    int getPort() {
        return port;
    }

    String getApiKey() {
        return apiKey;
    }

    void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    void execute() {
        try {
            Socket socket = new Socket(hostname, port);

            System.out.println(String.format(SUCCESSFUL_CONNECTION, port));

            new ReadThread(socket, this).start();
            new WriteThread(socket, this).start();

        } catch (UnknownHostException ex) {
            System.out.println(SERVER_NOT_FOUND_ERROR + ex.getMessage());
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public static void main(String args[]) {

        String hostname = "localhost";
        int port = 8080;
        Client c = new Client(hostname, port, "BwcEdAweWxjMiPBZEySuoiw8bEollRWTbOcdDBvk");
        c.execute();
    }
}