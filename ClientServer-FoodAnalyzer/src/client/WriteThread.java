package client;

import java.io.*;
import java.net.Socket;

public class WriteThread extends Thread {

    private static final String WRITING_TO_SERVER_ERROR = "Error writing to server: %s";
    private static final String OUTPUT_STREAM_ERROR = "Error getting output stream: %s";
    private static final String DISCONNECTION_REQUEST = "disconnect";

    private PrintWriter writer;
    private Socket socket;
    private Client client;

    WriteThread(Socket socket, Client client) {
        this.socket = socket;
        this.client = client;
        try {
            writer = new PrintWriter(socket.getOutputStream(), true);
        } catch (IOException ex) {
            System.out.println(String.format(OUTPUT_STREAM_ERROR, ex.getMessage()));
            ex.printStackTrace();
        }
    }

    @Override
    public void run() {
        BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
        writer.println(client.getApiKey());
        String request;

        try {
            do {
                request = console.readLine();
                writer.println(request);
            } while (!request.equalsIgnoreCase(DISCONNECTION_REQUEST));

            socket.close();
        } catch (IOException ex) {
            System.out.println(String.format(WRITING_TO_SERVER_ERROR, ex.getMessage()));
            ex.printStackTrace();
        }
    }
}