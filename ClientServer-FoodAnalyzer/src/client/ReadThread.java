package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class ReadThread extends Thread {

    private static final String SERVER_DISCONNECTION = "=> disconnected from server on localhost:%d";
    private static final String READING_FROM_SERVER_ERROR = "Error reading from server: %s";
    private static final String INPUT_STREAM_ERROR = "Error getting input stream: %s";
    private static final String DISCONNECTION_RESPONSE = "disconnect";

    private BufferedReader reader;
    private Socket socket;
    private Client client;

    ReadThread(Socket socket, Client client) {
        this.socket = socket;
        this.client = client;

        try {
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException ex) {
            System.out.println(String.format(INPUT_STREAM_ERROR, ex.getMessage()));
            ex.printStackTrace();
        }
    }

    @Override
    public void run() {
        try {
            String response;
            while (true) {
                response = reader.readLine();
                if (response.equalsIgnoreCase(DISCONNECTION_RESPONSE)) {
                    break;
                }
                System.out.println(response);
                System.out.flush();
            }
            System.out.println(String.format(SERVER_DISCONNECTION, client.getPort()));
            socket.close();
        } catch (IOException ex) {
            System.out.println(String.format(READING_FROM_SERVER_ERROR, ex.getMessage()));
            ex.printStackTrace();
        }
    }
}