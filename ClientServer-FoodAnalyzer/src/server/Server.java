package server;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import server.dto.Error.GeneralErrorInfo;
import server.dto.food_info.FoodInfo;
import server.dto.food_info.GeneralFoodInfo;
import server.dto.food_report.FoodReport;
import server.dto.food_report.Report;
import server.exception.ClientHandlerException;
import server.exception.InvalidFoodRequestException;
import server.exception.QRCReaderException;
import server.exception.QRCWriterException;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ConcurrentHashMap;

class Server {

    private static final String REQUEST_FOOD_BY_NAME_URL = "https://api.nal.usda.gov/ndb/search/?q=%s&api_key=%s";
    private static final String REQUEST_FOOD_BY_DBNUM_URL = "https://api.nal.usda.gov/ndb/reports/?ndbno=%s&type=f&api_key=%s";

    private static final String BARCODE_IMAGE_DIRECTORY = "./resources/foods/%s.png";

    private int port;
    private HttpClient httpClient;
    private ServerSocket serverSocket;

    private Map<String, ArrayList<FoodInfo>> localFoodInfo;
    private Map<String, FoodReport> localFoodReport;
    private Map<String, FoodInfo> localFoodUpc;

    Server(int port, HttpClient httpClient) {
        this.port = port;
        this.httpClient = httpClient;
        localFoodInfo = new ConcurrentHashMap<>();
        localFoodReport = new ConcurrentHashMap<>();
        localFoodUpc = new ConcurrentHashMap<>();
    }

    void start() {
        try {
            serverSocket = new ServerSocket(port);
            loadFoodFromFileSystem();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    List<FoodInfo> getFoodInfo(String apiKey, String food) {

        if (localFoodInfo.containsKey(food)) {
            return localFoodInfo.get(food);
        }

        localFoodInfo.put(food, new ArrayList<>());
        HttpRequest request = makeFoodRequest(food, apiKey);
        return httpClient.sendAsync(request, HttpResponse.BodyHandlers.ofString())
                .thenApply(HttpResponse::body)
                .thenApply(json -> {
                    try {
                        return new Gson().fromJson(json, GeneralFoodInfo.class)
                                .getList().getFoodInfoList();
                    } catch (NullPointerException | CompletionException e) {
                        throw new InvalidFoodRequestException(new Gson().fromJson(json, GeneralErrorInfo.class)
                                .getErrors().toString(), e);
                    }
                })
                .thenApply(list -> {
                    list.forEach(f -> {
                        localFoodInfo.get(food).add(f);
                        generateQRCodeImage(f, new Gson().toJson(f));
                    });
                    WriterReader.writeFoodInfoToFile(food, localFoodInfo.get(food));
                    return localFoodInfo.get(food);
                })
                .join();
    }

    FoodReport getFoodReport(String apiKey, String dbNumber) {

        if (localFoodReport.containsKey(dbNumber)) {
            return localFoodReport.get(dbNumber);
        }

        HttpRequest request = makeFoodReportRequest(dbNumber, apiKey);
        return httpClient.sendAsync(request, HttpResponse.BodyHandlers.ofString())
                .thenApply(HttpResponse::body)
                .thenApply(json -> {
                    try {
                        return new Gson().fromJson(json, Report.class).getReport().getFood();
                    } catch (NullPointerException | CompletionException e) {
                        throw new InvalidFoodRequestException(new Gson().fromJson(json, GeneralErrorInfo.class)
                                .getErrors().toString(), e);
                    }
                })
                .thenApply(object -> {
                    localFoodReport.put(dbNumber, object);
                    generateQRCodeImage(object, new GsonBuilder()
                            .excludeFieldsWithoutExposeAnnotation().create().toJson(object));
                    WriterReader.writeFoodReportToFile(dbNumber, object);
                    return object;
                }).join();
    }

    FoodInfo getFoodByBarcode(String upc) {
        return (localFoodUpc.getOrDefault(upc, null));
    }

    FoodInfo getFoodByBarcodeImage(String imageName) throws QRCReaderException, IOException {
        return new Gson()
                .fromJson(QRCodeGenerator
                        .readQRCode(String.format(BARCODE_IMAGE_DIRECTORY, imageName)), FoodInfo.class);
    }

    private HttpRequest makeFoodRequest(String food, String apiKey) {
        String URL = String.format(REQUEST_FOOD_BY_NAME_URL, food, apiKey);
        return HttpRequest.newBuilder().uri(URI.create(URL)).build();
    }

    private HttpRequest makeFoodReportRequest(String dbNumber, String apiKey) {
        String URL = String.format(REQUEST_FOOD_BY_DBNUM_URL, dbNumber, apiKey);
        return HttpRequest.newBuilder().uri(URI.create(URL)).build();
    }

    private void generateQRCodeImage(FoodInfo food, String jsonInfo) {
        if (!hasFoodWithUpc(food.getName(), food)) {
            try {
                QRCodeGenerator.generateQRCodeImage
                        (jsonInfo, String.format(BARCODE_IMAGE_DIRECTORY, getUpc(food.getName())));
            } catch (QRCWriterException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private String getUpc(String foodName) {
        String upcPart = foodName.substring(foodName.lastIndexOf(","));
        return upcPart.substring(upcPart.lastIndexOf(" ")).trim();
    }

    private boolean hasFoodWithUpc(String foodName, FoodInfo food) {
        if (food.getName().contains("UPC:")) {
            String upc = getUpc(foodName);
            if (!localFoodUpc.containsKey(upc)) {
                localFoodUpc.put(upc, food);
                return false;
            }
        }

        return true;
    }

    private void loadFoodFromFileSystem() {
        localFoodInfo = WriterReader.readFoodInfoFromFile();
        localFoodReport = WriterReader.readFoodReportFromFile();
        localFoodUpc = WriterReader.readFoodUpcsFromFolder();
    }

    void listen() throws IOException {
        while (true) {
            Socket clientConnection = null;
            try {
                clientConnection = serverSocket.accept();
                new Thread(new ClientHandler(clientConnection, this)).start();
            } catch (IOException | ClientHandlerException e) {
                if (clientConnection != null) {
                    clientConnection.close();
                }
            }
        }
    }

    public static void main(String[] args) throws IOException {
        final int port = 8080;
        Server server = new Server(port, HttpClient.newHttpClient());
        server.start();
        server.listen();
    }
}