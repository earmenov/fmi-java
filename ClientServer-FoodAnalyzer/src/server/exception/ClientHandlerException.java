package server.exception;

import java.io.IOException;

public class ClientHandlerException extends Exception {

    public ClientHandlerException(String message, IOException e) {
        super(message, e);
    }
}