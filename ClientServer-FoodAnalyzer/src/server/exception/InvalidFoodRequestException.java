package server.exception;

public class InvalidFoodRequestException extends RuntimeException {
    public InvalidFoodRequestException(String message, RuntimeException e) {
        super(message, e);
    }
}
