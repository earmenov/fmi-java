package server.exception;

public class QRCWriterException extends Exception {

    public QRCWriterException(String message, Exception e) {
        super(message, e);
    }
}
