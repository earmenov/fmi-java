package server.exception;

public class QRCReaderException extends Exception {

    public QRCReaderException(String message, Exception e) {
        super(message, e);
    }
}
