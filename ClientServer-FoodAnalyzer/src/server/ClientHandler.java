package server;

import server.dto.food_info.FoodInfo;
import server.enums.ServerCommands;
import server.exception.ClientHandlerException;
import server.exception.QRCReaderException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;


class ClientHandler implements Runnable {

    private static final String INVALID_REQUEST_PARAMETER = "Could not find anything with request parameter \"%s\"";
    private static final String CLIENT_INIT_ERROR = "Error occurred when initializing the client";
    private static final String CLIENT_HANDLER_ERROR = "Error in ClientHandler: ";

    private Socket client;
    private Server server;
    private BufferedReader input;
    private PrintWriter output;

    ClientHandler(Socket client, Server server) throws ClientHandlerException {
        this.client = client;
        this.server = server;

        try {
            this.output = new PrintWriter(client.getOutputStream(), true);
            this.input = new BufferedReader(new InputStreamReader(client.getInputStream()));
        } catch (IOException e) {
            throw new ClientHandlerException(CLIENT_INIT_ERROR, e);
        }
    }

    @Override
    public void run() {
        try {
            String apiKey = input.readLine().trim();
            String line;

            while (true) {
                line = input.readLine().trim();
                if (line.equals("disconnect")) {
                    break;
                }

                String[] clientInput = line.split(" ", 2);
                try {
                    ServerCommands command = ServerCommands.fromString(clientInput[0]);
                    String searchValue = clientInput[1];

                    switch (command) {
                        case GET_FOOD:
                            try {
                                server.getFoodInfo(apiKey, searchValue)
                                        .parallelStream().forEach(f -> output.println(f.printFood()));
                            } catch (RuntimeException e) {
                                output.println(e.getMessage());
                            }
                            break;
                        case GET_FOOD_REPORT:
                            try {
                                output.println(server.getFoodReport(apiKey, searchValue).printFood());
                            } catch (RuntimeException e) {
                                output.println(e.getMessage());
                            }
                            break;
                        case GET_FOOD_BY_BARCODE:
                            String[] searchByCommand = searchValue.split("=");
                            if (searchByCommand[0].trim().equals("--upc")) {
                                String upc = searchByCommand[1].trim();
                                FoodInfo response = server.getFoodByBarcode(upc);
                                output.println(response != null
                                        ? response.printFood()
                                        : String.format(INVALID_REQUEST_PARAMETER, upc));
                            } else if (searchByCommand[0].trim().equals("--img")) {
                                String img = searchByCommand[1].trim();
                                try {
                                    output.println(server.getFoodByBarcodeImage(img).printFood());
                                } catch (QRCReaderException | IOException e) {
                                    output.println(e.getMessage());
                                }
                            } else {
                                output.println("Unknown search type");
                            }
                            break;
                    }
                } catch (IllegalArgumentException e) {
                    output.println(e.getMessage());
                }
            }

            output.println(line);
            client.close();

        } catch (IOException e) {
            output.println(CLIENT_HANDLER_ERROR + e.getMessage());
            e.printStackTrace();
        }
    }
}