package server.dto.food_report;

public class Report {

    private GeneralFoodReport report;

    public GeneralFoodReport getReport() {
        return report;
    }

    public void setReport(GeneralFoodReport report) {
        this.report = report;
    }
}
