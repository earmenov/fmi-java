package server.dto.food_report;

import java.io.Serializable;

public class Ingredients implements Serializable {

    private String desc;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public String toString() {
        return "Ingredients: " + desc  + "\n";
    }
}
