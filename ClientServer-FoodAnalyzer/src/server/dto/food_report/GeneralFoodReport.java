package server.dto.food_report;

public class GeneralFoodReport {

    private FoodReport food;

    public FoodReport getFood() {
        return food;
    }

    public void setFood(FoodReport food) {
        this.food = food;
    }
}
