package server.dto.food_report;

import server.dto.food_info.FoodInfo;

import java.util.List;

public class FoodReport extends FoodInfo {

    private Ingredients ing;

    private List<Nutrients> nutrients;

    public Ingredients getIng() {
        return ing;
    }

    public void setIng(Ingredients ing) {
        this.ing = ing;
    }

    public List<Nutrients> getNutrients() {
        return nutrients;
    }

    public void setNutrients(List<Nutrients> nutrients) {
        this.nutrients = nutrients;
    }

    @Override
    public String printFood() {
        return super.printFood()
                + (getIng() != null ? ing.toString() : "") + (getNutrients() != null ? getNutrientsInfo() : "");
    }

    private String getNutrientsInfo() {
        final int numberOfIngNeeded = 5;
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < numberOfIngNeeded; i++) {
            sb.append(nutrients.get(i));
        }

        return sb.toString();
    }
}
