package server.dto.food_info;

import java.util.List;

public class ListInfo {

    private List<FoodInfo> item;

    ListInfo() {
    }

    public ListInfo(List<FoodInfo> item) {
        this.item = item;
    }

    public List<FoodInfo> getFoodInfoList() {
        return item;
    }

    public void setFoodInfoList(List<FoodInfo> item) {
        this.item = item;
    }
}
