package server.dto.food_info;

public class GeneralFoodInfo {

    private ListInfo list;

    GeneralFoodInfo() {}

    public GeneralFoodInfo(ListInfo list) {
        this.list = list;
    }

    public ListInfo getList() {
        return list;
    }

    public void setList(ListInfo list) {
        this.list = list;
    }
}
