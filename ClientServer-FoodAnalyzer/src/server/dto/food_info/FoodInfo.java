package server.dto.food_info;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

public class FoodInfo implements Serializable {

    @Expose
    private String name;
    @Expose
    private String ndbno;
    @Expose
    private String manu;

    public FoodInfo() {
    }

    public FoodInfo(String name, String ndbno, String manu) {
        this.name = name;
        this.ndbno = ndbno;
        this.manu = manu;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNdbno() {
        return ndbno;
    }

    public void setNdbno(String ndbno) {
        this.ndbno = ndbno;
    }

    public String getManu() {
        return manu;
    }

    public void setManu(String manu) {
        this.manu = manu;
    }

    public String printFood() {
        return String.format("Food name: %s\nNdbno: %s\nManu: %s\n", getName(), getNdbno(), getManu());
    }
}
