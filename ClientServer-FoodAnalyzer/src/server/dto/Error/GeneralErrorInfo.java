package server.dto.Error;

public class GeneralErrorInfo {

    private RequestErrors errors;

    public RequestErrors getErrors() {
        return errors;
    }

    public void setErrors(RequestErrors errors) {
        this.errors = errors;
    }
}
