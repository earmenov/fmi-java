package server.dto.Error;

import java.util.List;

public class RequestErrors {

    private List<Error> error;

    public List<Error> getError() {
        return error;
    }

    public void setError(List<Error> error) {
        this.error = error;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        for (Error e : error) {
            sb.append(String.format("status: %d\nmessage: %s\n", e.getStatus(), e.getMessage()));
        }

        return sb.toString();
    }
}
