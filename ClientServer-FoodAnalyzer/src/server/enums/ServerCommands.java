package server.enums;

public enum ServerCommands {
    GET_FOOD("get-food"),
    GET_FOOD_REPORT("get-food-report"),
    GET_FOOD_BY_BARCODE("get-food-by-barcode");

    private static final String UNKNOWN_COMMAND = "Unknown command";

    private String value;

    ServerCommands(String value) {
        this.value = value;
    }

    public String getCommand() {
        return value;
    }

    public static ServerCommands fromString(String text) {
        for (ServerCommands pt : ServerCommands.values()) {
            if (pt.value.equalsIgnoreCase(text)) {
                return pt;
            }
        }

        throw new IllegalArgumentException(UNKNOWN_COMMAND);
    }
}
