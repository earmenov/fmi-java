package server;

import com.google.gson.Gson;
import server.dto.food_info.FoodInfo;
import server.dto.food_report.FoodReport;
import server.exception.QRCReaderException;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

class WriterReader {

    private static final String FOOD_INFO_FILE_PATH = "./resources/food_info/food_info_data_%d.dat";
    private static final String FOOD_INFO_META_DATA_PATH = "./resources/food_info/info_objects_count.dat";

    private static final String FOOD_REPORT_FILE_PATH = "./resources/food_report/food_info_data_%d.dat";
    private static final String FOOD_REPORT_META_DATA_PATH = "./resources/food_report/report_objects_count.dat";

    private static final String FOOD_BARCODE_FOLDER = "./resources/foods/";
    private static int foodInfoCounter = 0;
    private static int foodReportCounter = 0;


    synchronized static void writeFoodInfoToFile(String key, ArrayList<FoodInfo> value) {
        try (ObjectOutputStream o = new ObjectOutputStream(new FileOutputStream(
                new File(String.format(FOOD_INFO_FILE_PATH, foodInfoCounter++))));
             BufferedWriter writer = new BufferedWriter(new FileWriter(FOOD_INFO_META_DATA_PATH))) {
            o.writeObject(new AbstractMap.SimpleEntry<>(key, value));
            writer.write(String.valueOf(foodInfoCounter));
        } catch (FileNotFoundException e) {
            try {
                new File(FOOD_INFO_FILE_PATH).createNewFile();
            } catch (IOException ex) {
                ex.printStackTrace();
                System.out.println(ex.getMessage());
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    synchronized static void writeFoodReportToFile(String key, FoodReport value) {
        try (ObjectOutputStream o = new ObjectOutputStream(new FileOutputStream(
                new File(String.format(FOOD_REPORT_FILE_PATH, foodReportCounter++))));
             BufferedWriter writer = new BufferedWriter(new FileWriter(FOOD_REPORT_META_DATA_PATH))) {
            o.writeObject(new AbstractMap.SimpleEntry<>(key, value));
            writer.write(String.valueOf(foodReportCounter));
        }  catch (FileNotFoundException e) {
            try {
                new File(FOOD_INFO_FILE_PATH).createNewFile();
            } catch (IOException ex) {
                ex.printStackTrace();
                System.out.println(ex.getMessage());
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    static Map<String, ArrayList<FoodInfo>> readFoodInfoFromFile() {
        Map<String, ArrayList<FoodInfo>> map = new ConcurrentHashMap<>();

        foodInfoCounter = getFoodsCounter(FOOD_INFO_META_DATA_PATH);
        int tempCounter = foodInfoCounter;

        while (tempCounter-- > 0) {
            try (ObjectInputStream o = new ObjectInputStream(
                    new FileInputStream(new File(String.format(FOOD_INFO_FILE_PATH, tempCounter))))) {
                Map.Entry<String, ArrayList<FoodInfo>> entry = (Map.Entry<String, ArrayList<FoodInfo>>) o.readObject();
                map.put(entry.getKey(), entry.getValue());
            } catch (IOException e) {
                System.out.println(e.getMessage());
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        return map;
    }

    static Map<String, FoodReport> readFoodReportFromFile() {
        Map<String, FoodReport> map = new ConcurrentHashMap<>();

        foodReportCounter = getFoodsCounter(FOOD_REPORT_META_DATA_PATH);
        int tempCounter = foodReportCounter;

        while (tempCounter-- > 0) {
            try (ObjectInputStream o = new ObjectInputStream(
                    new FileInputStream(new File(String.format(FOOD_REPORT_FILE_PATH, tempCounter))))) {
                Map.Entry<String, FoodReport> entry = (Map.Entry<String, FoodReport>) o.readObject();
                map.put(entry.getKey(), entry.getValue());
            } catch (IOException e) {
                System.out.println(e.getMessage());
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        return map;
    }

    private static int getFoodsCounter(String filePath) {
        File metaData = new File(filePath);
        if (metaData.exists()) {
            try (BufferedReader br = new BufferedReader(new FileReader(metaData))) {
                String line;
                return ((line = br.readLine()) != null) ? Integer.parseInt(line) : 0;
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }

        return 0;
    }

    static Map<String, FoodInfo> readFoodUpcsFromFolder() {
        Map<String, FoodInfo> map = new ConcurrentHashMap<>();
        try (Stream<Path> paths = Files.walk(Paths.get(FOOD_BARCODE_FOLDER))) {
            String removeFileExtensionRegex = "[.][^.]+$";
            paths
                    .filter(Files::isRegularFile)
                    .forEach(file -> {
                        try {
                            map.put(file.getFileName().toString().
                                            replaceFirst(removeFileExtensionRegex, ""), new Gson().
                                    fromJson(QRCodeGenerator.
                                            readQRCode(FOOD_BARCODE_FOLDER + file.getFileName().toString()),
                                            FoodInfo.class));
                        } catch (QRCReaderException | IOException e) {
                            System.out.println(e.getMessage());
                        }
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }

        return map;
    }

}
