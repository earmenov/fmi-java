package server;

import com.google.zxing.*;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.QRCodeReader;
import com.google.zxing.qrcode.QRCodeWriter;
import server.exception.QRCReaderException;
import server.exception.QRCWriterException;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;

class QRCodeGenerator {

    private static final String WRITER_ERROR = "Error occurred when writing to file \"%s\"";
    private static final String READER_ERROR = "Error occurred when reading file \"%s\"";

    synchronized static void generateQRCodeImage(String text, String filePath) throws QRCWriterException {
        QRCodeWriter qrCodeWriter = new QRCodeWriter();

        try {
            int imageWidth = 16, imageHeight = 16;
            String imageFormat = "PNG";
            BitMatrix bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, imageWidth, imageHeight);
            Path path = FileSystems.getDefault().getPath(filePath);
            MatrixToImageWriter.writeToPath(bitMatrix, imageFormat, path);
        } catch (WriterException | IOException e) {
            throw new QRCWriterException(String.format(WRITER_ERROR, filePath), e);
        }
    }

    static String readQRCode(String fileName) throws QRCReaderException, IOException {
        File file = new File(fileName);
        int startX = 0, startY = 0, offset = 0;
        BufferedImage image = ImageIO.read(file);
        int[] pixels = image.
                getRGB(startX, startY, image.getWidth(), image.getHeight(), null, offset, image.getWidth());
        RGBLuminanceSource source = new RGBLuminanceSource(image.getWidth(), image.getHeight(), pixels);
        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));

        QRCodeReader reader = new QRCodeReader();
        try {
            Result result = reader.decode(bitmap);
            return result.getText();
        } catch (NotFoundException | ChecksumException | FormatException e) {
            throw new QRCReaderException(String.format(READER_ERROR, fileName), e);
        }
    }

}
